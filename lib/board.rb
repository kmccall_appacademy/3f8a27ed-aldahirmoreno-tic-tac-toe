class Board
  attr_reader :grid

  def initialize(grid = [[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[1]] = mark if empty?(pos)
    winner
    #p grid
  end

  def empty?(pos)
    if grid[pos[0]][pos[1]] == nil
      true
    else
      false
    end
  end

  def winner
    if row == :X || column == :X || left_diagonal == :X || right_diagonal == :X
      return :X
    elsif row == :O || column == :O || left_diagonal == :O || right_diagonal == :O
      return :O
    end
    return nil
  end

  def row
    grid.each{|x|
      if x.uniq.length == 1 && x[0] != nil
        if x[0] == :X
          return :X
        else
          return :O
        end
      end
    }
  end

  def column
    counter = 0
    while counter < grid.length
      dummy_arr = []
      (0...grid.length).each{|x| dummy_arr.push(grid[x][counter])}
     counter += 1
     if dummy_arr.uniq.length == 1 && dummy_arr[0] != nil
       if dummy_arr[0] == :X
          return :X
       else
         return :O
       end
      end
    end
  end

  def left_diagonal
    counter = 0
    dummy_arr = []
    while counter < grid.length
      dummy_arr.push(grid[counter][counter])
    counter += 1
    end
    if dummy_arr.uniq.length == 1 && dummy_arr[0] != nil
      if dummy_arr[0] == :X
        return :X
      else
        return :O
      end
    end
  end

  def right_diagonal
    counter = 0
    dummy_arr = []
    while counter < grid.length
      dummy_arr.push(grid[-(counter+1)][counter])
    counter += 1
    end
    if dummy_arr.uniq.length == 1 && dummy_arr[0] != nil
      if dummy_arr[0] == :X
        return :X
      else
        return :O
      end
    end
  end

  def over?
    if winner != nil || grid.flatten.include?(nil) == false
      true
    else
      false
    end
  end

end
