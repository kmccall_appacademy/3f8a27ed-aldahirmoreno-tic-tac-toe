require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'


class Game
  attr_accessor :player_one
  attr_accessor :player_two
  attr_accessor :board

  def initialize(player_one,player_two)
    @player_one = HumanPlayer.new(player_one)
    @player_two = ComputerPlayer.new(player_two)
    @board = Board.new
  end

  def current_player
    @current_player = player_one
  end

  def play_turn
    while board.winner == nil
      if @current_player == player_one
        player_one.display(board)
        pos = player_one.get_move
        board.place_mark(pos, :X)
        p board.grid
        p @current_player
        switch_players!
      else
        player_two.display(board)
        pos = player_two.get_move
        p pos
        p board.grid[pos[0]][pos[1]]
        p board.place_mark(pos, :O)
        p 'The computer is playing'
        p board.grid
        switch_players!
      end
    end
  end


  def switch_players!
    if @current_player == player_one
      p "here"
      @current_player = player_two
    else
      @current_player = player_one
    end
  end

end
