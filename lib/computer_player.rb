class ComputerPlayer
  attr_reader :name
  attr_accessor :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def mark
    if row == :X || column == :X || left_diagonal == :X || right_diagonal == :X
      return :X
    elsif row == :O || column == :O || left_diagonal == :O || right_diagonal == :O
      return :O
    end
    return nil
  end

  def row
    board.grid.each{|x|
      if x.uniq.length == 2 && x.count(nil) == 1
        if x[0] == :X
          return :X
        else
          return :O
        end
      end
    }
  end

  def column
    counter = 0
    while counter < board.grid.length
      dummy_arr = []
      (0...board.grid.length).each{|x| dummy_arr.push(board.grid[x][counter])}
     counter += 1
     if dummy_arr.uniq.length == 2 && dummy_arr.count(nil) == 1
       if dummy_arr[0] == :X
          return :X
       else
         return :O
       end
      end
    end
  end

  def left_diagonal
    counter = 0
    dummy_arr = []
    while counter < board.grid.length
      dummy_arr.push(board.grid[counter][counter])
    counter += 1
    end
    if dummy_arr.uniq.length == 2 && dummy_arr.count(nil) == 1
      if dummy_arr[0] == :X
        return :X
      else
        return :O
      end
    end
  end

  def right_diagonal
    counter = 0
    dummy_arr = []
    while counter < board.grid.length
      dummy_arr.push(board.grid[-(counter+1)][counter])
    counter += 1
    end
    if dummy_arr.uniq.length == 2 && dummy_arr.count(nil) == 1
      if dummy_arr[0] == :X
        return :X
      else
        return :O
      end
    else
      :O
    end
  end

  def get_move
    if mark != nil
      fakeboard = board.clone
      fakeboard.grid.each_with_index{|x,x_index|
        x.each_with_index{|y,y_index|
          fakeboard2 = fakeboard.clone
          fakeboard2.grid[x_index][y_index] = mark
          if fakeboard2.winner != nil
            return [y_index,x_index]
          end
        }
      }
    else
      board.grid.each_with_index{|x, x_index|
        x.each_with_index{|y, y_index|
          if board.grid[x_index][y_index] == nil
            return [y_index, x_index]
          end
        }
      }
    end
  end

end
