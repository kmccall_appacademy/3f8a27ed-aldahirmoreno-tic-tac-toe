class HumanPlayer
  attr_reader :name
  attr_reader :move

  def initialize(name)
    @name = name
  end

  def get_move
    # puts "where would you like to move?(ex. 0,0): "
    # input = gets.chomp
    # input_arr1 = input.split(",")
    # input_arr2 =
    # input_final = input_arr.map{|x| x.to_i}
    puts "where would you like to move?: "
    input1 = gets.chomp
    puts "where would you like to move?: "
    input2 = gets.chomp
    raise "Wrong move" if (input1.to_i < 0 || input1.to_i > 2) || (input2.to_i < 0 || input2.to_i > 2)
    @move = [input1.to_i, input2.to_i]
  end

  def display(board)
    p board.grid
  end
end
